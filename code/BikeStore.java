//Johnny Hoang [2036759]
public class BikeStore {
    public static void main(String[] args){
        Bicycle[] arrayOfBicycles = new Bicycle[4];
        arrayOfBicycles[0]= new Bicycle("Toyota", 12, 100);
        arrayOfBicycles[1]= new Bicycle("Honda", 19, 77);
        arrayOfBicycles[2]= new Bicycle("Ford", 21, 85);
        arrayOfBicycles[3]= new Bicycle("Lexus", 23, 90);

        for (int i=0; i<arrayOfBicycles.length; i++){
            System.out.println(arrayOfBicycles[i]);
        }
    }
}
